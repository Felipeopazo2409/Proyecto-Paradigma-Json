package paquete.Modelos;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

public class Departamento{
	private int numero_depto;
	private String nombre;
	private int cant_trabajadores;
	public Departamento(int numero_depto,String nombre,int cantidad_trabajadores) {
		this.numero_depto = numero_depto;
		this.nombre = nombre;
		this.cant_trabajadores = cantidad_trabajadores;
	}
	//METODOS SETTERS Y GETTERS
	public void setNumero_depto(int numero_depto) {
		this.numero_depto = numero_depto;
	}
	public int getNumero_depto() {
		return numero_depto;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNombre() {
		return nombre;
	}
	public void setCantidad_trabajadores(int cantidad) {
		this.cant_trabajadores = cantidad;
	}
	public int getCantidad_trabajadores() {
		return cant_trabajadores;
	}
	
	public static JSONObject getDeptoByN(JSONArray deptos, String n) {
		JSONObject dep = null;
		for(int i=0; i < deptos.length(); i++) {
			if( deptos.getJSONObject(i).get("numero_depto") != null && n.equals(deptos.getJSONObject(i).get("numero_depto").toString()) ) {
				dep = deptos.getJSONObject(i);
				break;
			}
		}
		return dep;
	}
	
	public static JSONArray deleteDeptoByN(JSONArray deptos, String n) {
		if(deptos != null) {
			for(int i=0; i<deptos.length(); i++) {
				if( n.equals(deptos.getJSONObject(i).get("numero_depto").toString()) ) {				
					deptos.remove(i);
				}
			}
		}
		return deptos;
	}
	
	public static JSONObject departamentoToJsonObject(Departamento departamento) {
		String jsonInString = new Gson().toJson(departamento);
		JSONObject mJSONObject = new JSONObject(jsonInString);
		return mJSONObject;
	}
	
	
}

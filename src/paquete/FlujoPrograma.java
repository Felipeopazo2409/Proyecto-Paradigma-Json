package paquete;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import org.json.JSONArray;
import org.json.simple.parser.ParseException;

import paquete.Utils.FileUtil;
import paquete.Ventanas.VentanaAdministrador;
import paquete.Ventanas.VentanaInformacion;
import paquete.Ventanas.VentanaPrincipal;
import paquete.Ventanas.VentanaTrabajador;

public class FlujoPrograma {
	public VentanaPrincipal ventana_main;
	public VentanaAdministrador ventana_admin;
	public VentanaTrabajador ventana_trabajadores;
	public VentanaInformacion ventana_informacion_general;
	
	public static String JSON_FILE_TRABAJADORES = "data/Trabajadores.json";
	public static String JSON_FILE_DEPARTAMENTOS = "data/Departamentos.json";
	
	public JSONArray trabajadores = new JSONArray();
	public JSONArray departamentos = new JSONArray();
	
	
	public FlujoPrograma() throws IOException, ParseException {
		CrearVentanas();
		funcionalidades_botones_ventana_main();
		funcionalidades_botones_ventana_admin(); 
		funcionalidades_botones_ventana_trabajadores();
		funcionalidades_botones_ventana_informacion();
		
	}

	
	public void CrearVentanas() throws IOException, ParseException {
	    
		ventana_main = new VentanaPrincipal();
		ventana_admin = new VentanaAdministrador();
		ventana_trabajadores = new VentanaTrabajador();
		
		try {
			trabajadores = FileUtil.read(JSON_FILE_TRABAJADORES);
		} catch (IOException | ParseException e1) {
			// TODO Bloque catch generado automáticamente
			e1.printStackTrace();
		}
		
		try {
			departamentos = FileUtil.read(JSON_FILE_DEPARTAMENTOS);
		} catch (IOException | ParseException e1) {
			// TODO Bloque catch generado automáticamente
			e1.printStackTrace();
		}
		int cantidad_trabajadores = trabajadores.length();
		int cantidad_depto = departamentos.length();
		
		
		ventana_informacion_general = new VentanaInformacion(cantidad_depto,cantidad_trabajadores);
		
	}
	
	public void funcionalidades_botones_ventana_main() {
		ventana_main.menu.boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventana_main.setVisible(false);
				ventana_admin.setVisible(true);
				ventana_trabajadores.setVisible(false);
				ventana_informacion_general.setVisible(false);
			}
		});
		
		ventana_main.menu.boton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventana_main.setVisible(false);
				ventana_admin.setVisible(false);
				ventana_trabajadores.setVisible(true);
				ventana_informacion_general.setVisible(false);
			}
		});
		
		ventana_main.menu.boton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventana_main.setVisible(false);
				ventana_admin.setVisible(false);
				ventana_trabajadores.setVisible(false);
				ventana_informacion_general.setVisible(true);
			}
		});
		
		ventana_main.menu.boton4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
	}
	
	public void funcionalidades_botones_ventana_admin() {
		ventana_admin.admin.volver_menu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventana_main.setVisible(true);
				ventana_admin.setVisible(false);
			}
		});
	}
	
	public void funcionalidades_botones_ventana_trabajadores() {
	
		ventana_trabajadores.panel.volver_menu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventana_main.setVisible(true);
				ventana_admin.setVisible(false);
				ventana_trabajadores.setVisible(false);
			}
		});
	}
	
	public void funcionalidades_botones_ventana_informacion() {
		 ventana_informacion_general.panel_informacion.boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventana_main.setVisible(true);
				ventana_admin.setVisible(false);
				ventana_trabajadores.setVisible(false);
				ventana_informacion_general.setVisible(false);
			}
		 });
	}
}

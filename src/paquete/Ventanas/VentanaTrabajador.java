package paquete.Ventanas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;

import paquete.Modelos.Trabajador;
import paquete.Paneles.Trabajador.PanelConsultarDatos;
import paquete.Paneles.Trabajador.PanelEliminarTrabajador;
import paquete.Paneles.Trabajador.PanelInsertarTrabajador;
import paquete.Paneles.Trabajador.PanelLiquidacionSueldo;
import paquete.Paneles.Trabajador.PanelModificarTrabajador;
import paquete.Paneles.Trabajador.PanelTrabajador;
import paquete.Utils.FileUtil;

//import java.util.logging.Logger;

@SuppressWarnings("serial")
public class VentanaTrabajador extends JFrame {
	
	private static final String JSON_FILE = "data/Trabajadores.json";
	
	public PanelTrabajador panel;
	private JScrollPane scrollpane;
	public PanelInsertarTrabajador panelInsertarTrabajador;
	public PanelModificarTrabajador panelModificarTrabajador;
	public PanelEliminarTrabajador panelEliminarTrabajador;
	private PanelConsultarDatos panelConsultarTrabajador;
	private PanelLiquidacionSueldo panelLiquidacionTrabajador;
	public Trabajador trabajador;
	public JSONArray trabajadores;
	
	public int pos;
	public int pos_eliminar;
	public int cont = 0;
	public int largo = 0;
	
	public VentanaTrabajador() throws IOException, ParseException {
		panel = new PanelTrabajador();
		
		trabajadores = new JSONArray();
		setSize(780, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(false);
		setLocationRelativeTo(null);
		Inicializar();
		navegacion();
		volver_atras();
		insertarDatos();
		modificar_informacion();
		eliminar_trabajador();
		consultar_datos();
		
	}
	private void Inicializar() {
		scrollpane = new JScrollPane();
		scrollpane.setBounds(1, 1, 779, 500);
		scrollpane.setViewportView(panel);
		add(scrollpane);
	}
	
	private void navegacion() {//Navegamos por los diferentes paneles
		// Instancia de paneles
		panelInsertarTrabajador = new PanelInsertarTrabajador();
		panelModificarTrabajador = new PanelModificarTrabajador();
		panelEliminarTrabajador = new PanelEliminarTrabajador();
		panelConsultarTrabajador = new PanelConsultarDatos();
		panelLiquidacionTrabajador = new PanelLiquidacionSueldo();
	
		// Eventos de los botones
		
		// pinchar boton para ir a panel ingresar un trabajador
		panel.ingresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(panelInsertarTrabajador);
			}
		});

		// Pinchar Boton para ir a panel modificar un trabajador
		panel.modificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(panelModificarTrabajador);

			}
		});

		// Pinchar Boton para ir a panel Eliminar un trabajador
		panel.eliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(panelEliminarTrabajador);
			}
		});
		// Pinchar Boton para ir a panel Consultar Datos de un trabajador
		panel.consultar_datos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(panelConsultarTrabajador);
			}
		});
		// Pinchar Boton para ir a panel Generar Liquidacion de sueldo
		panel.generar_liquidacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(panelLiquidacionTrabajador);
			}
		});
	}

	private void volver_atras() {//Eventos para llegar al menú principal
		panelInsertarTrabajador.cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(panel);
			}
		});

		panelModificarTrabajador.cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(panel);
			}
		});

		panelEliminarTrabajador.cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(panel);
			}
		});
		panelConsultarTrabajador.volver_menu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(panel);
			}
		});
		panelLiquidacionTrabajador.volver_menu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(panel);
			}
		});

	}

	private void insertarDatos() {

		panelInsertarTrabajador.guardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					insertarTrabajadores();
				} catch (IOException | ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				vaciar_campos_trabajador();
				JOptionPane.showMessageDialog(null, "Trabajador ingresado Exitosamente");
			}
		});

	}

	private void insertarTrabajadores() throws IOException, ParseException  {
		String nombre = panelInsertarTrabajador.campoNombre.getText();
		String apellidoP = panelInsertarTrabajador.campoApellidoPaterno.getText();
		String apellidoM = panelInsertarTrabajador.campoApellidoMaterno.getText();
		String obtener_rut = panelInsertarTrabajador.campoRut.getText();
		String fecha = panelInsertarTrabajador.campoNacimiento.getText();
		String tipo_contrato = panelInsertarTrabajador.contrato.getSelectedItem().toString();
		String obtener_salario = panelInsertarTrabajador.campoSalario.getText();
		String departamento = panelInsertarTrabajador.campoDepartamento.getText();
		
		trabajador = new Trabajador(
				nombre, 
				apellidoP, 
				apellidoM, 
				Integer.parseInt(obtener_rut), 
				fecha, 
				tipo_contrato, 
				Integer.parseInt(obtener_salario), 
				departamento
		);
		
		trabajadores = FileUtil.read(JSON_FILE);
		trabajadores.put(Trabajador.trabajadorToJsonObject(trabajador));

		FileUtil.write(JSON_FILE, trabajadores);
		
	}

	private void modificar_informacion() {
		
		panelModificarTrabajador.buscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vaciar_campos_trabajador();
				String obtener_rut = panelModificarTrabajador.campoRut.getText();
				
				try {
					trabajadores = FileUtil.read(JSON_FILE);
				} catch (IOException | ParseException e1) {
					// TODO Bloque catch generado automáticamente
					e1.printStackTrace();
				}
				
				JSONObject trab = Trabajador.getTrabajadorByRut(trabajadores, obtener_rut);
				
				if (trab != null) {
					mostrar_campos_trabajador(trab);
					editar_datos_trabajador();
					
					JOptionPane.showMessageDialog(null,"Trabajador Encontrado exitosamente");
				}else {
					JOptionPane.showMessageDialog(null,"Error al encontrar trabajador,Intente Nuevamente");
				}
			}
			
		});
		
		//Guardamos la información del trabajador modificado
		panelModificarTrabajador.guardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//Obtengo los valores de los campos TextField
				String nombre = panelModificarTrabajador.campoNombre.getText();
				String apellidoP = panelModificarTrabajador.campoApellidoPaterno.getText();
				String apellidoM = panelModificarTrabajador.campoApellidoMaterno.getText();
				String obtener_rut = panelModificarTrabajador.campoRut.getText();
				String fecha = panelModificarTrabajador.campoNacimiento.getText();
				String tipo_contrato = panelModificarTrabajador.campo_contrato.getText();
				String obtener_salario = panelModificarTrabajador.campoSalario.getText();
				String departamento = panelModificarTrabajador.campoDepartamento.getText();
				
				
				try {
					trabajadores = FileUtil.read(JSON_FILE);
				} catch (IOException | ParseException e1) {
					// TODO Bloque catch generado automáticamente si falla al leer el archivo
					e1.printStackTrace();
				}
				
				trabajadores = Trabajador.deleteTrabajadorByRut(trabajadores, obtener_rut);
				
				trabajador = new Trabajador(
						nombre, 
						apellidoP, 
						apellidoM, 
						Integer.parseInt(obtener_rut), 
						fecha, 
						tipo_contrato, 
						Integer.parseInt(obtener_salario), 
						departamento
				);
				
				trabajadores.put(Trabajador.trabajadorToJsonObject(trabajador));
				FileUtil.write(JSON_FILE, trabajadores);
				
				//Mensaje De alerta
				vaciar_campos_trabajador();
				JOptionPane.showMessageDialog(null, "Trabajador Modificado exitosamente");
				
			}
		});
		
		
	}
	private void eliminar_trabajador() {
		panelEliminarTrabajador.buscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vaciar_campos_trabajador();
				String obtener_rut = panelEliminarTrabajador.campo_rut.getText();
				
				try {
					trabajadores = FileUtil.read(JSON_FILE);
				} catch (IOException | ParseException e1) {
					// TODO Bloque catch generado automáticamente Si falla al leer el archivo
					e1.printStackTrace();
				}
				
				JSONObject trab = Trabajador.getTrabajadorByRut(trabajadores, obtener_rut);
				
				if(trab != null) {
					mostrar_campos_trabajador(trab);
				
					JOptionPane.showMessageDialog(null, "Se ha encontrado el trabajador");
					
					trabajadores = Trabajador.deleteTrabajadorByRut(trabajadores, obtener_rut);
					FileUtil.write(JSON_FILE, trabajadores);
				}else {
					JOptionPane.showMessageDialog(null, "El trabajador no existe");
				}
					
			}
		});
		
		panelEliminarTrabajador.eliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//eliminar desde el paso anterior;
				vaciar_campos_trabajador();
				JOptionPane.showMessageDialog(null,"Trabajador eliminado exitosamente");
				
			}
		});
		
	}
		
	private void consultar_datos() {
		
		panelConsultarTrabajador.buscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String obtener_rut = panelConsultarTrabajador.campo_rut.getText();
				
				try {
					trabajadores = FileUtil.read(JSON_FILE);
				} catch (IOException | ParseException e1) {
					// TODO Bloque catch generado automáticamente
					e1.printStackTrace();
				}
				
				JSONObject trab = Trabajador.getTrabajadorByRut(trabajadores, obtener_rut);
				
				if (trab != null) {
					mostrar_campos_trabajador(trab);
					no_editar_datos_trabajador();
					JOptionPane.showMessageDialog(null,"Trabajador Encontrado exitosamente");
				}else {
					JOptionPane.showMessageDialog(null,"Error al encontrar trabajador,Intente Nuevamente");
				}
			}
		});
		
	}
	
	private void vaciar_campos_trabajador() {
		panelInsertarTrabajador.campoNombre.setText("");
		panelInsertarTrabajador.campoApellidoPaterno.setText("");
		panelInsertarTrabajador.campoApellidoMaterno.setText("");
		panelInsertarTrabajador.campoNacimiento.setText("");
		panelInsertarTrabajador.campoRut.setText("");
		panelInsertarTrabajador.campoSalario.setText("");
		panelInsertarTrabajador.campoDepartamento.setText("");
	}
	
	private void mostrar_campos_trabajador(JSONObject trab) {
		panelConsultarTrabajador.campoNombre.setText(trab.get("nombre").toString());
		panelConsultarTrabajador.campoApellidoPaterno.setText(trab.get("apellido_paterno").toString());
		panelConsultarTrabajador.campoApellidoMaterno.setText(trab.get("apellido_materno").toString());
		panelConsultarTrabajador.campoNacimiento.setText(trab.get("fecha_de_nacimiento").toString());
		panelConsultarTrabajador.campo_contrato.setText(trab.get("tipo_contrato").toString());
		panelConsultarTrabajador.camposalario.setText(trab.get("salario").toString());
		panelConsultarTrabajador.campodepartamento.setText(trab.get("departamento").toString());
	}
	
	private void no_editar_datos_trabajador() {
		panelConsultarTrabajador.campoNombre.setEditable(false);
		panelConsultarTrabajador.campoApellidoPaterno.setEditable(false);
		panelConsultarTrabajador.campoApellidoMaterno.setEditable(false);
		panelConsultarTrabajador.campoNacimiento.setEditable(false);
		panelConsultarTrabajador.campo_contrato.setEditable(false);
		panelConsultarTrabajador.camposalario.setEditable(false);
		panelConsultarTrabajador.campodepartamento.setEditable(false);
	}
	
	private void editar_datos_trabajador() {
		panelConsultarTrabajador.campoNombre.setEditable(true);
		panelConsultarTrabajador.campoApellidoPaterno.setEditable(true);
		panelConsultarTrabajador.campoApellidoMaterno.setEditable(true);
		panelConsultarTrabajador.campoNacimiento.setEditable(true);
		panelConsultarTrabajador.campo_contrato.setEditable(true);
		panelConsultarTrabajador.camposalario.setEditable(true);
		panelConsultarTrabajador.campodepartamento.setEditable(true);
	}
			
}
	

	

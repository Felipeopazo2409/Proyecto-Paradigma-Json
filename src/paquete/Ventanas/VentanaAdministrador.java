package paquete.Ventanas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;

import paquete.MenuPrincipal;
import paquete.Modelos.Departamento;
import paquete.Paneles.PanelAdministracion;
import paquete.Paneles.Departamento.PanelEliminarDepto;
import paquete.Paneles.Departamento.PanelInsertarDepto;
import paquete.Paneles.Departamento.PanelMostrarDepto;
import paquete.Utils.FileUtil;

@SuppressWarnings("serial")
public class VentanaAdministrador extends JFrame {
	
	private static final String JSON_FILE = "data/Departamentos.json";
	
	private JScrollPane scrollpane;
	public PanelAdministracion admin;
	public MenuPrincipal menu;
	public PanelInsertarDepto panel_insertar;
	public PanelEliminarDepto panel_eliminar;
	public PanelMostrarDepto mostrar_info;
	public ArrayList<Departamento> lista_departamentos = new ArrayList<Departamento>();
	ArrayList<String> nombres_departamentos;
	public Departamento departamento;
	public JSONArray deptos;
	
	public VentanaAdministrador() {
		deptos = new JSONArray();
		
		setSize(780,500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(false);
		setLocationRelativeTo(null);
		 admin = new PanelAdministracion();
		 scrollpane = new JScrollPane();
		 scrollpane.setBounds(1,1,779,500);
		 scrollpane.setViewportView(admin);
		 add(scrollpane);
		 navegacion();
		 volver_atras();
		 insertar_datos();
		 mostrar_informacion();
		 eliminar_departamento();
	}
	
	
	private void navegacion() {
		panel_insertar = new PanelInsertarDepto();
		panel_eliminar = new PanelEliminarDepto();
		mostrar_info = new PanelMostrarDepto();
		//Pincho boton para insertar un nuevo departamento
		admin.ingresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(panel_insertar);
			}
		});
		
		//Pincho boton para eliminar Departamento
		
		admin.eliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(panel_eliminar);
			}
		});
		
		//pincho boton para mostrar informacion de todos los departamentos
		
		admin.mostrarInformacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(mostrar_info);
			}
		});

	}
	
	private void volver_atras() {
		panel_insertar.volver_menu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(admin);
			}
		});
		panel_eliminar.cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(admin);
			}
		});
		
		mostrar_info.volver_menu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scrollpane.setViewportView(admin);
			}
		});
	}
	
	private void mostrar_informacion() {
		
		try {
			deptos = FileUtil.read(JSON_FILE);
		} catch (IOException | ParseException e1) {
			// TODO Bloque catch generado automáticamente
			e1.printStackTrace();
		}
		
		nombres_departamentos = new ArrayList<String>();
		mostrar_info.mostrar_lista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int y=250;
				for(int i=0; i < deptos.length(); i++) {
					
					JSONObject dep = deptos.getJSONObject(i);
					
					JLabel depto = new JLabel(dep.get("nombre").toString());
					depto.setBounds(140,y,200,40);
					mostrar_info.add(depto);
					nombres_departamentos.add(dep.get("nombre").toString());
					
					y+=40;
				}
			}
		});
		
	}
	private void insertar_datos() {
		panel_insertar.guardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String n_depto = panel_insertar.campo1.getText();
				String nombre = panel_insertar.campo2.getText();
				String numero_trabajadores= panel_insertar.campo3.getText();
				departamento = new Departamento(
						Integer.parseInt(n_depto), 
						nombre, 
						Integer.parseInt(numero_trabajadores)
				);
				
				try {
					deptos = FileUtil.read(JSON_FILE);
				} catch (IOException | ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				deptos.put(Departamento.departamentoToJsonObject(departamento));

				FileUtil.write(JSON_FILE, deptos);
				
				JOptionPane.showMessageDialog(null, "Departamento Ingresado Exitosamente");
				panel_insertar.campo1.setText("");
				panel_insertar.campo2.setText("");
				panel_insertar.campo3.setText("");
			}
		});
	}
	
	private void eliminar_departamento() {
		
		panel_eliminar.buscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String obtener_ndepto = panel_eliminar.input.getText();
				
				try {
					deptos = FileUtil.read(JSON_FILE);
				} catch (IOException | ParseException e1) {
					// TODO Bloque catch generado automáticamente Si falla al leer el archivo
					e1.printStackTrace();
				}
				
				JSONObject dep = Departamento.getDeptoByN(deptos, obtener_ndepto);
				
				if(dep != null) {
					
					panel_eliminar.campo1.setText(dep.get("nombre").toString());
					panel_eliminar.campo2.setText(dep.get("cant_trabajadores").toString());
					
					deptos = Departamento.deleteDeptoByN(deptos, obtener_ndepto);
					FileUtil.write(JSON_FILE, deptos);
					JOptionPane.showMessageDialog(null,"Se ha encontrado el departamento");
				}else {
					JOptionPane.showMessageDialog(null,"El N° que ha ingresado no existe, Intente nuevamente");
				}
				
			}
		});
		
		panel_eliminar.eliminar_depto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JOptionPane.showMessageDialog(null, "Departamento Eliminado Correctamente");
				panel_eliminar.input.setText("");
				panel_eliminar.campo1.setText("");
				panel_eliminar.campo2.setText("");
			}
		});
		
	}
	
}

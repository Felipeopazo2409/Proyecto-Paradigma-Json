package paquete.Utils;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;

public class FileUtil {
	
	public static void write(String file_url, JSONArray arreglo) {
		
		FileWriter file;
		String arregloJson = "{ \"myArrayList\": " + arreglo.toString() + "}";
		
		try {
			file = new FileWriter(file_url);
			file.write(arregloJson);
			file.flush();
			file.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
		
	}

	public static JSONArray read(String file_url) throws IOException, ParseException {
		
		String content = new String(Files.readAllBytes(Paths.get(file_url)));
		JSONObject empleadosLeidos1 = new JSONObject();
		if(!content.isEmpty()) {
			empleadosLeidos1 = new JSONObject(content);
			empleadosLeidos1.get("myArrayList");
			return (JSONArray) empleadosLeidos1.get("myArrayList");
		} else {
			return new JSONArray();
		}
		
	}
	

}
